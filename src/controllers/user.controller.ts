import { Request, Response } from "express";
import user_model from "../modules/user.model.js";

export default class UserController {
    static createUser = async (req: Request, res: Response) => {
        const user = await user_model.create(req.body);
        res.status(200).json(user);
    };

    static getAllUsers = async (req: Request, res: Response) => {
        const users = await user_model.find().select(`_id 
                                                first_name 
                                                last_name 
                                                email 
                                                phone`);
        res.status(200).json(users);
    };

    static getUsersByPage = async (req: Request, res: Response) => {
        const users = await user_model
            .find()
            .select(
                `_id 
                                              first_name 
                                              last_name 
                                              email 
                                              phone`
            )
            .skip(
                Number(req.params.pageNumber) > 0
                    ? (Number(req.params.pageNumber) - 1) *
                          Number(req.params.nPerPage)
                    : 0
            )
            .limit(Number(req.params.nPerPage));
        res.status(200).json(users);
    };

    static getUserById = async (req: Request, res: Response) => {
        const user = await user_model.findById(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    };

    static updateUserById = async (req: Request, res: Response) => {
        const user = await user_model.findByIdAndUpdate(
            req.params.id,
            req.body,
            { new: true, upsert: false }
        );
        res.status(200).json(user);
    };

    static deleteUserById = async (req: Request, res: Response) => {
        const user = await user_model.findByIdAndRemove(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    };
}
