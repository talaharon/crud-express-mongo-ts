import express from "express";
import morgan from "morgan";
import cors from "cors";
import { printError, errorResponse, logErrorMiddleware } from "../middleware/errors.handler.js";
import user_router from "../routes/user.router.js";
import common_router from "../routes/common.router.js";
import { logger } from "../middleware/common.middleware.js";
class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGeneral();
        this.configMiddlewares();
        this.configRoutes();
        this.configErrorHandlers();
    }

    configErrorHandlers() {
        const { ERR_PATH = "./logs/users.errors.log" } = process.env;
        this.app.use(printError);
        this.app.use(logErrorMiddleware(ERR_PATH));
        this.app.use(errorResponse);
    }

    private configGeneral() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    private configRoutes() {
        this.app.use("/api/users", user_router);
        this.app.use("*", common_router);
    }

    private configMiddlewares() {
        const { LOG_PATH = './logs/users.http.log.txt' } = process.env;
        this.app.use(logger(LOG_PATH));
    }
}

export default new App().app;
