// require('dotenv').config();
import log from '@ajar/marker';
import {connect_db} from './db/mongoose.connection.js';
import app from './app-config/app.js';



const { PORT = 8080,HOST = 'localhost', DB_URI='mongodb://localhost:27017/crud-demo' } = process.env;

//start the express api server
;(async ()=> {
  //connect to mongo db
  await connect_db(DB_URI);  
  await app.listen(Number(PORT),HOST);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log);