/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import {
    userExists,
    validateUserCreation,
    validateUserUpdate,
} from "../middleware/user.middleware.js";
import UserController from "../controllers/user.controller.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post(
    "/",
    validateUserCreation,
    userExists(false),
    raw(UserController.createUser)
);

// GET ALL USERS
router.get("/", raw(UserController.getAllUsers));

router.get(
    "/pagination/:pageNumber/:nPerPage",
    raw(UserController.getUsersByPage)
);

// GETS A SINGLE USER
router.get("/:id", raw(UserController.getUserById));

// UPDATES A SINGLE USER
router.put(
    "/:id",
    validateUserUpdate,
    userExists(true),
    raw(UserController.updateUserById)
);

// DELETES A USER
router.delete("/:id", userExists(true), raw(UserController.deleteUserById));

export default router;
