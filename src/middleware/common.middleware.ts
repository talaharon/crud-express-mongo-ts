import { RequestHandler } from "express";
import fsp from "fs/promises";
export const logger =
    (path: string): RequestHandler =>
        async (req, res, next) => {
            await fsp.appendFile(
                path,
                "[" + +Date.now() + "] " + req.method + " " + req.path + "\n"
            );
            next();
        };
