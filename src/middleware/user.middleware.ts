import { RequestHandler } from "express";
import yup from "yup";
import UserExistsException from "../exceptions/UserNotExist.exception.js";
import ValidationException from "../exceptions/Validation.exception.js";
import user_model from "../modules/user.model.js";

const creationSchema = yup.object().shape({
    first_name: yup
        .string()
        .matches(/^[a-zA-Z]+$/)
        .min(3)
        .max(10)
        .required(),
    last_name: yup
        .string()
        .matches(/^[a-zA-Z]+$/)
        .min(3)
        .max(10)
        .required(),
    email: yup.string().email().required(),
    phone: yup.string().matches(/^\d+$/).required(),
});

const updateSchema = yup.object().shape({
    first_name: yup
        .string()
        .matches(/^[a-zA-Z]+$/)
        .min(3)
        .max(10),
    last_name: yup
        .string()
        .matches(/^[a-zA-Z]+$/)
        .min(3)
        .max(10),
    email: yup.string().email(),
    phone: yup.string().matches(/^\d+$/),
});

export const validateUserCreation: RequestHandler = (req, res, next) => {
    creationSchema
        .validate(req.body)
        .then(() => next())
        .catch(() =>
            next(
                new ValidationException(`Invalid Parameters for user creation`)
            )
        );
};

export const validateUserUpdate: RequestHandler = (req, res, next) => {
    updateSchema
        .validate(req.body)
        .then(() => next())
        .catch(() =>
            next(new ValidationException(`Invalid Parameters for user update`))
        );
};

export const userExists =
    (expectedResult: boolean): RequestHandler =>
    async (req, res, next) => {
        const count = await user_model.countDocuments({ _id: req.params.id });
        if ((count > 0 && expectedResult) || (count <= 0 && !expectedResult)) {
            next();
        } else {
            const errMsg = expectedResult
                ? `User ${req.params.id} does not exist`
                : `User ${req.params.id} already exists`;
            next(new UserExistsException(errMsg));
        }
    };
